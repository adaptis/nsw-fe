// trigger document ready
// handleIEBrowser();


window.addEventListener('scroll', function () {
  // hideSidebar();
});


// trigger window onload
window.onload = function () {
  // detectLoadpage();
};


// trigger window resize
var timeout;
// Listen for resize events
window.addEventListener('resize', function (event) {
  // If timer is null, reset it to 66ms and run your functions. Otherwise, wait until timer is cleared
  if (!timeout) {
    timeout = setTimeout(function () {
      // Reset timeout
      timeout = null;

      // fire event
    }, 66);
  }
}, false);

//================================================================================

